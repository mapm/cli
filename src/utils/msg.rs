use colour::*;
use mapm::result::MapmErr;
use mapm::result::MapmErr::*;

pub fn print_err(err: &MapmErr) {
    match err {
        ProblemErr(err) => {
            e_red_ln!("Problem error: {}", err);
        }
        ContestErr(err) => {
            e_red_ln!("Contest error: {}", err);
        }
        TemplateErr(err) => {
            e_red_ln!("Template error: {}", err);
        }
        SolutionErr(err) => {
            e_red_ln!("Solution error: {}", err);
        }
    }
}

// INTERNAL ERRORS

pub static VIEW_NOT_PROBLEMERR: &str =
    "Internal software error: `mapm view` failed with an error type other than `ProblemErr`
Please report this message to the developers.";

pub static FIND_NOT_PROBLEMERR: &str =
    "Internal software error: `mapm find` failed with an error type other than `ProblemErr`
Please report this message to the developers.";
