use crate::utils::profile::get_profile;
use std::path::PathBuf;

pub fn master_problem_dir() -> PathBuf {
    dirs::config_dir().unwrap().join("mapm").join("problems")
}

pub fn problem_dir() -> Option<PathBuf> {
    get_profile().map(|profile| {
        dirs::config_dir()
            .unwrap()
            .join("mapm")
            .join("problems")
            .join(profile)
    })
}

pub fn template_dir() -> PathBuf {
    dirs::config_dir().unwrap().join("mapm").join("templates")
}

pub fn scripts_dir() -> PathBuf {
    dirs::config_dir().unwrap().join("mapm").join("scripts")
}
