use crate::utils::dirs::{problem_dir, scripts_dir, template_dir};
use crate::utils::{msg, preview};

use colour::*;
use mapm::problem::{Solutions, Vars};
use std::process::Command;
use std::{env, fs};

pub fn contest(contest_result: &mapm::contest::ContestFetchResult) {
    match contest_result {
        Ok(contest) => {
            let contest_msgs = contest.compile(&template_dir());
            if let Some(err) = &contest_msgs.err {
                msg::print_err(err);
            };
            for result in contest_msgs.latexmk_results {
                match result {
                    Ok(msg) => {
                        println!("{}", msg);
                    }
                    Err(err) => {
                        e_red_ln!("{}", err);
                    }
                }
            }
        }
        Err(errs) => {
            if let Some(err) = &errs.contest_err {
                msg::print_err(err);
                quit::with_code(exitcode::DATAERR);
            }
            if let Some(errs) = &errs.problem_errs {
                for problem_err in errs {
                    e_red_ln!("Errors for problem `{}`:", &problem_err.0);
                    for err in &problem_err.1 {
                        msg::print_err(err);
                    }
                }
            }
        }
    }
}

pub fn exec_script(script_name: &str) {
    let script_path = scripts_dir().join(script_name);
    if script_path.exists() {
        if cfg!(windows) {
            Command::new("powershell")
                .arg(
                    fs::read_to_string(script_path)
                        .expect("Could not read post-build script file to string"),
                )
                .spawn()
                .expect("Could not execute post-build script");
        } else if cfg!(unix) {
            Command::new("sh")
                .arg(script_path)
                .spawn()
                .expect("Could not execute post-build script");
        }
    }
}

pub fn preview(problem_name: &str) {
    let contest_yml = &preview::preview_yml(problem_name);
    let cwd = env::current_dir().expect("Could not get current directory");
    let cache_dir = dirs::cache_dir().expect("Could not find cache directory");
    let preview_dir = cache_dir.join("mapm").join(problem_name);
    if !preview_dir.is_dir() {
        fs::create_dir_all(&preview_dir).expect("Could not create directory");
    }
    env::set_current_dir(&preview_dir).expect("Could not set working directory");
    fs::write("contest.yml", contest_yml).expect("Could not write to `contest.yml`");
    let contest_result =
        mapm::contest::fetch_contest("contest.yml", &problem_dir().unwrap(), &template_dir());
    contest(&contest_result);
    exec_script("preview");
    env::set_current_dir(&cwd).expect("Could not set working directory");
}

pub fn preview_all(problems: &[(String, Vars, Option<Solutions>)]) {
    let cwd = env::current_dir().expect("Could not get current directory");
    let cache_dir = dirs::cache_dir().expect("Could not find cache directory");
    let preview_all_dir = cache_dir.join("mapm").join("preview-all");
    if !preview_all_dir.is_dir() {
        fs::create_dir_all(&preview_all_dir).expect("Could not create directory");
    }
    env::set_current_dir(&preview_all_dir).expect("Could not set working directory");
    let contest = preview::preview_all_contest(problems);
    let compile_results = contest.compile(template_dir());
    if let Some(err) = compile_results.err {
        msg::print_err(&err);
    }
    for latexmk_result in compile_results.latexmk_results {
        match latexmk_result {
            Ok(latexmk_result) => println!("{}", latexmk_result),
            Err(latexmk_result) => {
                e_red_ln!("{}", latexmk_result);
            }
        }
    }
    exec_script("preview-all");
    env::set_current_dir(&cwd).expect("Could not set working directory");
}
