use std::env;
use std::io::Write;
use std::process::{Command, Stdio};

use mapm::problem::*;

use ansi_term::Colour::*;
use ansi_term::Style;

pub fn problems_to_string(problems: &[(String, Vars, Option<Solutions>)]) -> String {
    let mut problem_display = String::new();
    let color: bool = env::var("NO_COLOR").is_err();
    let g_bold_style: Option<Style> = Some(Green.bold());
    let b_style: Option<Style> = Some(Blue.normal());
    let b_bold_style: Option<Style> = Some(Blue.bold());
    let r_style: Option<Style> = Some(Red.normal());
    let r_bold_style: Option<Style> = Some(Red.bold());
    let mut append_str = |s: &str, sty_opt: Option<Style>| {
        if color {
            match sty_opt {
                Some(sty) => {
                    problem_display.push_str(&sty.paint(s).to_string());
                }
                None => {
                    problem_display.push_str(s);
                }
            }
        } else {
            problem_display.push_str(s);
        }
    };
    let mut problem_index = 0;
    for (problem_name, vars, solutions) in problems {
        problem_index += 1;
        if problem_index > 1 {
            append_str("\n\n", None);
        }
        append_str(&format!("-- {} --", problem_name), g_bold_style);
        for (key, val) in vars {
            append_str("\n", None);
            append_str(key, b_style);
            append_str(": ", None);
            let mut idx = 0;
            for string in val.lines() {
                idx += 1;
                if idx == 1 {
                    append_str(string, None);
                } else {
                    append_str("\n  ", None);
                    append_str(&str::repeat(" ", key.chars().count()), None);
                    append_str(string, None);
                }
            }
        }
        match solutions {
            Some(solutions) => {
                append_str("\n", None);
                append_str("solutions", b_bold_style);
                append_str(": ", None);
                if solutions.is_empty() {
                    append_str("empty", r_bold_style);
                } else {
                    let mut index: u32 = 0;
                    for solution in solutions {
                        index += 1;
                        append_str("\n  ", None);
                        append_str(&(index.to_string() + "."), r_style);
                        let mut key_index = 0;
                        for (key, val) in solution {
                            key_index += 1;
                            if key_index == 1 {
                                append_str(" ", None);
                            } else {
                                append_str("\n", None);
                                append_str(&str::repeat(" ", 5), None);
                            }
                            append_str(key, b_style);
                            append_str(": ", None);
                            let mut idx = 0;
                            for string in val.lines() {
                                idx += 1;
                                if idx > 1 {
                                    append_str("\n", None);
                                    append_str(
                                        &str::repeat(
                                            " ",
                                            index.to_string().len() + key.chars().count() + 6,
                                        ),
                                        None,
                                    );
                                }
                                append_str(string, None);
                            }
                        }
                    }
                }
            }
            None => {
                append_str("\n", None);
            }
        }
    }
    problem_display
}

pub fn display(string: &str) {
    match env::var("PAGER") {
        Ok(pager) => {
            let mut process = match Command::new(&pager).stdin(Stdio::piped()).spawn() {
                Err(msg) => panic!("Couldn't spawn {pager}: {msg}"),
                Ok(process) => process,
            };
            if let Err(e) = process.stdin.as_ref().unwrap().write_all(string.as_bytes()) {
                if e.kind() == std::io::ErrorKind::BrokenPipe {
                    if matches!(std::env::var("NO_BROKEN_PIPE"), Err(_)) {
                        println!("The pipe from stdin to your pager was broken.");
                        println!("This is nothing to be concerned about and is absolutely normal if you didn't load the entire file into your pager.");
                        println!("The error message is reproduced below:");
                        println!("\t{}", e);
                        println!("If you would like to never see this input again, set the `NO_BROKEN_PIPE` environment variable.");
                        println!("(It doesn't matter what value you give it.)");
                    }
                } else {
                    panic!("Couldn't write to `{pager}` stdin: {e}");
                }
            }
            process.wait().expect("`wait` failed");
        }
        Err(_) => {
            println!("{}", string);
        }
    }
}
