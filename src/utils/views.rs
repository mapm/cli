use crate::utils::msg;

use mapm::problem::Views::*;
use mapm::problem::{fetch_problem, Solutions, Vars, Views};
use mapm::result::MapmErr::*;

use std::path::Path;

use colour::*;

// Because of clap, we won't have both hide and show be both

pub fn parse_views(hide: Option<Vec<String>>, show: Option<Vec<String>>) -> Views {
    match hide {
        Some(views) => Hide(views),
        None => match show {
            Some(views) => Show(views),
            None => Hide(vec![]),
        },
    }
}

pub fn problem_names_to_problems(
    profile: &str,
    problem_names: &Vec<String>,
    views: Views,
) -> Vec<(String, Vars, Option<Solutions>)> {
    let config = dirs::config_dir().unwrap();
    let mut problems: Vec<(String, Vars, Option<Solutions>)> = Vec::new();
    for problem_name in problem_names {
        let problem_result = fetch_problem(
            problem_name,
            &Path::new(&config)
                .join("mapm")
                .join("problems")
                .join(profile),
        );
        match problem_result {
            Ok(problem) => {
                let name = problem.name.clone();
                let filtered_problem = problem.filter_keys(&views);
                problems.push((name, filtered_problem.0, filtered_problem.1));
            }
            Err(err) => match err {
                ProblemErr(msg) => {
                    e_yellow_ln!("{}", msg);
                }
                _ => {
                    e_magenta_ln!("{}", msg::VIEW_NOT_PROBLEMERR);
                    quit::with_code(exitcode::SOFTWARE);
                }
            },
        }
    }
    problems
}
