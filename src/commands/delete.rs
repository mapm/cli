use crate::utils::dirs;
use std::fs;

pub fn delete(names: Vec<String>) {
    for name in names {
        delete_one(name);
    }
}

fn delete_one(name: String) {
    fs::remove_file(dirs::problem_dir().unwrap().join(format!("{}.yml", name)))
        .expect("Failed to remove problem");
}
