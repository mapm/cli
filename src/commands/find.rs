use crate::utils::dirs::problem_dir;
use crate::utils::profile::get_profile;

pub fn find(filters: Vec<String>, hide: Option<Vec<String>>, show: Option<Vec<String>>) {
    let views = crate::utils::views::parse_views(hide, show);
    let filters = crate::utils::filter::parse_filters(&filters);
    let problem_names = crate::utils::filter::filtered_names(&filters, &problem_dir().unwrap());
    let problem_display = crate::utils::cli_display::problems_to_string(
        &crate::utils::views::problem_names_to_problems(
            &get_profile().unwrap(),
            &problem_names,
            views,
        ),
    );
    crate::utils::cli_display::display(&problem_display);
}
