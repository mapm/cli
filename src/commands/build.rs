use crate::utils::build;
use crate::utils::dirs::{problem_dir, template_dir};
use crate::utils::profile::get_profile;
use std::path::Path;

pub fn build(contests: Vec<String>) {
    for contest in contests {
        let contest_result = mapm::contest::fetch_contest(
            Path::new(&contest),
            &problem_dir().unwrap(),
            &template_dir(),
        );
        if let Ok(contest) = &contest_result {
            build::exec_script(&contest.template.name)
        };
        build::contest(&contest_result);
    }
}

pub fn preview(problems: Vec<String>) {
    for problem in problems {
        build::preview(&problem);
    }
}

pub fn preview_all(filters: Vec<String>, hide: Option<Vec<String>>, show: Option<Vec<String>>) {
    let views = crate::utils::views::parse_views(hide, show);
    let filters = crate::utils::filter::parse_filters(&filters);
    let problem_names = crate::utils::filter::filtered_names(&filters, &problem_dir().unwrap());
    let filtered_problems = crate::utils::views::problem_names_to_problems(
        &get_profile().unwrap(),
        &problem_names,
        views,
    );

    build::preview_all(&filtered_problems);
}
